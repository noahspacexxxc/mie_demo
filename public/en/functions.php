<?php
/**
 * MTS functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package MTS
 */

if ( ! function_exists( 'mts_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function mts_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on MTS, use a find and replace
		 * to change 'mts' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'mts', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'mts' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'mts_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'mts_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function mts_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'mts_content_width', 640 );
}
add_action( 'after_setup_theme', 'mts_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function mts_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'mts' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'mts' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'mts_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function mts_scripts() {


    wp_enqueue_style( 'mts-style', get_stylesheet_uri(), array() );

	wp_enqueue_script( 'mts-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'mts-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

    wp_enqueue_script( 'mts-customizer', get_template_directory_uri() . '/js/customizer.js', array(), '20151215', true );
    
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'mts_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


/* 固定ページのみビジュアルエディアを非表示化 */
function disable_visual_editor_in_page() {
	global $typenow;
	if( $typenow == 'page' ){
		add_filter('user_can_richedit', 'disable_visual_editor_filter');
	}
}
function disable_visual_editor_filter(){
	return false;
}
add_action('load-post.php', 'disable_visual_editor_in_page');
add_action('load-post-new.php', 'disable_visual_editor_in_page');


/* 固定ページに不要な<p>タグ<br>タグを入らないようにする */
add_filter('the_content', 'wpautop_filter', 9);
function wpautop_filter($content) {
global $post;
$remove_filter = false;
$arr_types = array('page'); //適用させる投稿タイプを指定
$post_type = get_post_type( $post->ID );
if (in_array($post_type, $arr_types)) $remove_filter = true;
if ( $remove_filter ) {
remove_filter('the_content', 'wpautop');
remove_filter('the_excerpt', 'wpautop');
}
return $content;
}



// 管理画面：投稿一覧でタームでの絞り込みを可能にする
add_action( 'restrict_manage_posts', 'add_post_product_cat_restrict_filter' );
function add_post_product_cat_restrict_filter() {

    /* カスタム投稿タイプを取得 */
    global $post_type;

    /* タームで絞り込みを行いたいカスタム投稿タイプの場合 */
    if ( 'exhibition' == $post_type ) {

        /* 検索's'の変数がセットされている場合 */
        if ( isset( $_GET['s'] ) ) {

            /* 選択されたターム情報を取得し、変数へ代入 */
            $select_product_cat = $_GET['exhibition_year'];
        }

        /* タームのセレクトボックスを生成 */ ?>
<select name="exhibition_year">
    <option value="">カテゴリー指定なし</option>
    <?php

        /* 最上位のターム一覧を取得。空のタームも含む。 */
        $terms = get_terms( 'exhibition_year',
                           array( 'hide_empty' => false, 'parent' => 0 )
                          );

        /* 取得したタームをオプションに変換 */
        foreach ( $terms as $term ) {

            /* 選択されたタームと同じタームの場合 */
            ( $term->slug == $select_product_cat ) ? $selected = 'selected' : $selected = '';

    ?>
    <option value="<?php echo $term->slug; ?>" <?php echo $selected; ?>><?php echo $term->name; ?></option>

    <?php /* 今回は2階層目まで表示したいので子を持っているターム一覧も取得 */
            $child_terms = get_terms( 'exhibition_year',
                                     array( 'hide_empty' => false, 'parent' => $term->term_id )
                                    );

            /* 子タームを取得できた場合 */
            if( !empty( $child_terms ) ) {

                /* 子タームをオプションに変換 */
                foreach( $child_terms as $child_term ) {

                    /* 選択されたタームと同じタームの場合 */
                    ( $child_term->slug == $select_product_cat ) ? $selected = 'selected' : $selected = '';

    ?>
    <option value="<?php echo $child_term->slug; ?>" <?php echo $selected; ?>>ー <?php echo $child_term->name; ?></option>
    <?php
                }
            }
        } ?>
</select>
<?php
    }
}



/*********************
OGPタグ/Twitterカード設定を出力
*********************/
 /*
function my_meta_ogp() {
    if( is_front_page() || is_home() || is_singular() ){
        global $post;
      //  $ogp_title = '';
        $ogp_descr = '';
        $ogp_url = '';
        $ogp_img = '';
        $insert = '';

        if( is_singular() ) { //記事＆固定ページ
            setup_postdata($post);
            $ogp_title = $post->post_title;
            $ogp_descr = mb_substr(get_the_excerpt(), 0, 100);
            $ogp_url = get_permalink();
            wp_reset_postdata();
        } elseif ( is_front_page() || is_home() ) { //トップページ
            $ogp_title = get_bloginfo('name');
            $ogp_descr = get_bloginfo('description');
            $ogp_url = home_url();
        }

        //og:type
        $ogp_type = ( is_front_page() || is_home() ) ? 'website' : 'article';

        //og:image
        if ( is_singular() && has_post_thumbnail() ) {
            $ps_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
            $ogp_img = $ps_thumb[0];
        } else {
            $ogp_img = 'http://taiwantranslation.com/wp-content/themes/mts/img/og.jpg';
        }

        //出力するOGPタグをまとめる
        $insert .= '<meta property="og:title" content="'.esc_attr($ogp_title).'" />' . "\n";
        $insert .= '<meta property="og:description" content="'.esc_attr($ogp_descr).'" />' . "\n";
        $insert .= '<meta property="og:type" content="'.$ogp_type.'" />' . "\n";
        $insert .= '<meta property="og:url" content="'.esc_url($ogp_url).'" />' . "\n";
        $insert .= '<meta property="og:image" content="'.esc_url($ogp_img).'" />' . "\n";
        $insert .= '<meta property="og:site_name" content="'.esc_attr(get_bloginfo('name')).'" />' . "\n";
        $insert .= '<meta name="twitter:card" content="summary_large_image" />' . "\n";
        $insert .= '<meta property="og:locale" content="zh_TW" />' . "\n";
        $insert .= '<meta property="og:locale" content="ja_JP" />' . "\n";

        //facebookのapp_id（設定する場合）
        $insert .= '<meta property="fb:app_id" content="ここにappIDを入力">' . "\n";
        //app_idを設定しない場合ここまで消す

        echo $insert;
    }
} //END my_meta_ogp

add_action('wp_head','my_meta_ogp');//headにOGPを出力
*/


function remove_posted_on( $output ){
    return str_replace("posted on", "", $output);
}
add_filter( 'wpp_post', 'remove_posted_on');


// 管理画面（展示会のデザイン調整）
function my_admin_style() {
    echo '<style>
    .exhibition-link { border-bottom: 3px solid #ddd; }
    #acf-group_5e9283957004d .CodeMirror-scroll { min-height: 100px; }
    #acf-group_5ebac8c9c3cfb .CodeMirror-scroll { min-height: 100px; }
  </style>'.PHP_EOL;
}
add_action('admin_print_styles', 'my_admin_style');