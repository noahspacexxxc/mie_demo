<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MTS
 */

if ( get_current_blog_id() === 1 ) {
    get_header();
} elseif ( get_current_blog_id() === 2 ) {
    get_header("zh");
} elseif ( get_current_blog_id() === 5 ) {
    get_header("en-new");
}

?>



<!--   ▼日本語TOP-------------------------------------------------------------- -->

<?php
if ( get_current_blog_id() === 1 ) {
?>
 
<div id="primary" class="content-area site-top">
    <main id="main" class="site-main">


        <h1 class="top-hero">
            <img class="disp-large" src="<?php bloginfo('template_directory'); ?>/img/hero_lower.png" alt="私たちは、中国語・台湾語を専門とする翻訳会社です。">
            <img class="disp-small" src="<?php bloginfo('template_directory'); ?>/img/hero_lower_sp.png" alt="私たちは、中国語・台湾語を専門とする翻訳会社です。">
        </h1>

        
        <?php
            while ( have_posts() ) :
            the_post();

            get_template_part( 'template-parts/content', 'page' );

            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) :
            comments_template();
            endif;

            endwhile; // End of the loop.
        ?>

        <section class="frontpage top-news">
            <h2 class="top-news__heading">NEWS</h2>          
            <div class="top-news-list">
              <div class="top-news-list__item">
              	<h3>営業日のお知らせ</h3>
          <?php $query = new wp_query(array( 'showposts' => 3, 'cat' => '13' )); if($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                <a class="clearfix" href="<?php the_permalink(); ?>">
                  <?php  
                  if (has_post_thumbnail()) {
                     the_post_thumbnail(array(218,999));
                    }else {
                     echo '<img src="'. get_site_url().'/assets/img/common/thumb_noline.jpg" alt="thumbnail" />';
                    }                              
                  ?>
                  <span class="news-title"><span><?php the_title();?></span><span class="date"><?php echo get_post_time('Y.m.d'); ?></span></span>        
                </a>                           
          <?php endwhile; endif?>              
              </div>
              <div class="top-news-list__item">
              	<h3>サービス</h3>
          <?php $query = new wp_query(array( 'showposts' => 3, 'cat' => '14' )); if($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                <a class="clearfix" href="<?php the_permalink(); ?>">
                  <?php  
                  if (has_post_thumbnail()) {
                     the_post_thumbnail(array(218,999));
                    }else {
                     echo '<img src="'. get_site_url().'/assets/img/common/thumb_noline.jpg" alt="thumbnail" />';
                    }                              
                  ?>
                  <span class="news-title"><span><?php the_title();?></span><span class="date"><?php echo get_post_time('Y.m.d'); ?></span></span>        
                </a>                            
          <?php endwhile; endif?>              
              </div>
              <div class="top-news-list__item">
              	<h3>イベント関連</h3>
          <?php $query = new wp_query(array( 'showposts' => 3, 'cat' => '15' )); if($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                <a class="clearfix" href="<?php the_permalink(); ?>">
                  <?php  
                  if (has_post_thumbnail()) {
                     the_post_thumbnail(array(218,999));
                    }else {
                     echo '<img src="'. get_site_url().'/assets/img/common/thumb_noline.jpg" alt="thumbnail" />';
                    }                              
                  ?>
                  <span class="news-title"><span><?php the_title();?></span><span class="date"><?php echo get_post_time('Y.m.d'); ?></span></span>        
                </a>                            
          <?php endwhile; endif?>              
              </div>                                      
            </div>
            <div class="top-news-btn"><a class="top-news-btn__link" href="/news">もっと見る</a></div>
        </section><!-- .top-news -->
        <div class="cta-box">
          <div class="container">
             <div>
                <h2><span>お問い合わせ</span>中華圏ビジネスに効く中国語翻訳サービスを、台湾よりサポートいたします。</h2>
                <p>経験豊かな翻訳コーディネーターが、お客様のビジネスに最適な翻訳プランをご案内いたします。ご相談は無料。お気軽にお問い合わせください。</p>
             </div>
             <div>
                <div class="phone-bg">
                    <p>日本</p>
                    <p class="phone">06-6195-4750</p>
                    <p>台湾</p>
                    <p class="phone">02-2765-2925</p>
                    <p class="time">受付時間／日本時間　平日10:00~19:00<br />受付時間／台湾時間　平日9:00~18:00</p>                
                </div>
                <a href="/translation_quote"><span>&#8594; 問い合わせる</span></a>
             </div>          
          </div>
        </div>
    </main> 
    <!-- #main -->

</div><!-- #primary -->

<script type='text/javascript' src='/wp-content/themes/mts/js/jquery.matchHeight.js'></script>
<script type="text/javascript">
    jQuery(function ($) {

        //▼高さを揃える
        // J3翻訳分野のブロック高さを揃える
        $('.top-navi-jp__item').matchHeight();


    });
</script>


<!--   ▼中国語TOP-------------------------------------------------------------- -->

<?php
} elseif ( get_current_blog_id() === 2 ) {
?>
<div id="primary" class="content-area site-top">
    <main id="main" class="site-main">
        <div class="top-hero">
            <h1><img class="c-sub-hero__image disp-large" src="/wp-content/themes/mts/img/zh/hero_lower_zh.png" alt="米耶翻譯是日文的翻譯專家"><img class="c-sub-hero__image disp-small" src="/wp-content/themes/mts/img/zh/hero_lower_sp_zh.png" alt="米耶翻譯是日文的翻譯專家"></h1>
        </div>

        <?php
            while ( have_posts() ) :
            the_post();

            get_template_part( 'template-parts/content', 'page' );

            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) :
            comments_template();
            endif;

            endwhile; // End of the loop.
        ?>

        <section class="frontpage top-news">
          <h2 class="top-news__heading">NEWS</h2>
          <div class="top-news-list">
            <div class="top-news-list__item">
              <h3>服務公告</h3>
              <?php $query = new wp_query(array( 'showposts' => 3, 'cat' => '8' )); if($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                    <a class="clearfix" href="<?php the_permalink(); ?>">
                      <?php
                      if (has_post_thumbnail()) {
                         the_post_thumbnail(array(218,999));
                        } else {
                         echo '<img src="'. get_site_url().'/assets/img/common/thumb_noline.jpg" alt="thumbnail" />';
                        }
                      ?>
                      <span class="news-title"><span><?php the_title();?></span><span class="date"><?php echo get_post_time('Y.m.d'); ?></span></span>
                    </a> 
              <?php endwhile; endif?>
            </div>
            <div class="top-news-list__item">
              <h3>活動公告</h3>
              <?php $query = new wp_query(array( 'showposts' => 3, 'cat' => '9' )); if($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                    <a class="clearfix" href="<?php the_permalink(); ?>">
                      <?php  
                      if (has_post_thumbnail()) {
                         the_post_thumbnail(array(218,999));
                        }else {
                         echo '<img src="'. get_site_url().'/assets/img/common/thumb_noline.jpg" alt="thumbnail" />';
                        }                              
                      ?>
                      <span class="news-title"><span><?php the_title();?></span><span class="date"><?php echo get_post_time('Y.m.d'); ?></span></span>        
                    </a>                            
              <?php endwhile; endif?>
            </div>
            <div class="top-news-list__item">
              <h3>最新消息</h3>
              <?php $query = new wp_query(array( 'showposts' => 3, 'cat' => '1' )); if($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                    <a class="clearfix" href="<?php the_permalink(); ?>">
                      <?php  
                      if (has_post_thumbnail()) {
                         the_post_thumbnail(array(218,999));
                        }else {
                         echo '<img src="'. get_site_url().'/assets/img/common/thumb_noline.jpg" alt="thumbnail" />';
                        }                              
                      ?>
                      <span class="news-title"><span><?php the_title();?></span><span class="date"><?php echo get_post_time('Y.m.d'); ?></span></span>        
                    </a>                            
              <?php endwhile; endif?>
            </div>
          </div>
          <div class="top-news-btn"><a class="top-news-btn__link" href="/zh/news">查看更多消息</a></div>
        </section><!-- .top-news -->

        <div class="cta-wrap">
           <div class="cta-container">
               <div class="cta-description">
                   <p class="cta-main-des text-weight-bold line-height-large c-tac mb-5x">
                       米耶為您提供最佳的翻譯方案
                   </p>
                   <p class="cta-sub-des c-tac line-height-medium">
                     協助您達成商務目標的翻譯服務，立即體驗
                   </p>
               </div>
               
               <div class="cta-tel-content">
                   <div class="cta-tel-box">
                       <span class="cta-tel">02-2765-2925</span>
                   </div>
               </div>
               <p class="c-tac line-height-medium">
                 週一至週五&nbsp;AM&nbsp;9:00-PM&nbsp;6:00
               </p>
               <div class="clearfix">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="col-sm-6">
                            <a href="https://taiwantranslation.com/zh/translation_quote" class="cta-contact-button button button-size-full mb-3x">
                                立即洽詢
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <a href="https://taiwantranslation.com/zh/trial" class="cta-trial-button button button-size-full mb-3x">
                                試譯服務
                            </a>
                        </div>
                    </div>
                </div>
           </div>
        </div>
    </main> 
    <!-- #main -->

</div><!-- #primary -->

<script type='text/javascript' src='/wp-content/themes/mts/js/jquery.matchHeight.js'></script>
<script type="text/javascript">
    jQuery(function ($) {

        //▼高さを揃える
        // J3翻訳分野のブロック高さを揃える
        $('.top-media-list__inner').matchHeight();


    });
</script>


<!--   -------------------------------------------------------------- -->
<!--   ▼English TOP-------------------------------------------------------------- -->

<?php
} elseif ( get_current_blog_id() === 5 ) {
?>
<div id="primary-en" class="content-area site-top">
    <main id="main" class="site-main">
        <?php
            while ( have_posts() ) :
            the_post();

            get_template_part( 'template-parts/content', 'page' );

            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) :
            comments_template();
            endif;

            endwhile; // End of the loop.
        ?>

    </main> 
    <!-- #main -->

</div><!-- #primary -->

<script type='text/javascript' src='/wp-content/themes/mts/js/jquery.matchHeight.js'></script>
<script type="text/javascript">
    jQuery(function ($) {

        //▼高さを揃える
        // J3翻訳分野のブロック高さを揃える
        $('.top-media-list__inner').matchHeight();


    });
</script>


<?php  }  ?>
<!--   -------------------------------------------------------------- -->




<?php
if ( get_current_blog_id() === 1 ) {
    get_footer();
} elseif ( get_current_blog_id() === 2 ) {
    get_footer("zh");
} elseif ( get_current_blog_id() === 5 ) {
    get_footer("en-new");
}
