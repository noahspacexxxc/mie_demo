<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package MTS
 */

if ( get_current_blog_id() === 1 ) {
    get_header();
} elseif ( get_current_blog_id() === 2 ) {
    get_header("zh");
} elseif ( get_current_blog_id() === 5 ) {
    get_header("en-new");
}
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title" style="text-align: center; padding: 1.0em 0;"><?php esc_html_e( '404 Not Found .', 'mts' ); ?></h1>
				</header><!-- .page-header -->
				
				<div class="c-contact-btn" style="padding: 60px 0 140px;">
				    <a class="c-contact-btn__link" href="/">TOP PAGE</a>
				</div>

				<div class="page-content"  style="display: none">
					<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'mts' ); ?></p>

					<?php
					get_search_form();

					the_widget( 'WP_Widget_Recent_Posts' );
					?>

					<div class="widget widget_categories">
						<h2 class="widget-title"><?php esc_html_e( 'Most Used Categories', 'mts' ); ?></h2>
						<ul>
							<?php
							wp_list_categories( array(
								'orderby'    => 'count',
								'order'      => 'DESC',
								'show_count' => 1,
								'title_li'   => '',
								'number'     => 10,
							) );
							?>
						</ul>
					</div><!-- .widget -->



				</div><!-- .page-content -->
				
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
if ( get_current_blog_id() === 1 ) {
    get_footer();
} elseif ( get_current_blog_id() === 2 ) {
    get_footer("zh");
} elseif ( get_current_blog_id() === 5 ) {
    get_footer("en-new");
}
