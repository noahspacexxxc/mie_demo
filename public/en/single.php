<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package MTS
 */

if ( get_current_blog_id() === 1 ) {
    get_header();
} elseif ( get_current_blog_id() === 2 ) {
    get_header("zh");
}

?>
    
    <style>
        @media screen and (max-width: 1000px) {
            .c-page-heading {
                margin: 1em 0 0.5em;
        }
        

    </style>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		
            <div class="c-sub-hero">
                <img class="c-sub-hero__image disp-large" src="/wp-content/themes/mts/img/hero_lower.png" alt="">
                <img class="c-sub-hero__image disp-small" src="/wp-content/themes/mts/img/hero_lower_sp.png" alt="">
            </div>            

		

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			//if ( comments_open() || get_comments_number() ) :
			//	comments_template();
			//endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->
	
<script type="text/javascript">
    jQuery(function ($) {
        
        $('.cat-links').each(function(){
            var txt = jQuery(this).html();
            $(this).html(txt.replace(/Posted in/,''));
        });
        
    });
</script>


<?php
    get_sidebar();
    
    if ( get_current_blog_id() === 1 ) {
        get_footer();
    } elseif ( get_current_blog_id() === 2 ) {
        get_footer("zh");
    }