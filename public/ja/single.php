<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package MTS
 */

if ( get_current_blog_id() === 1 ) {
    get_header();
} elseif ( get_current_blog_id() === 2 ) {
    get_header("zh");
}

?>
    
    <style>
        @media screen and (max-width: 1000px) {
            .c-page-heading {
                margin: 1em 0 0.5em;
        }
        

    </style>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		
            <div class="c-sub-hero">
                <img class="c-sub-hero__image disp-large" src="/wp-content/themes/mts/img/hero_lower.png" alt="">
                <img class="c-sub-hero__image disp-small" src="/wp-content/themes/mts/img/hero_lower_sp.png" alt="">
            </div>            

		

		<?php
		while ( have_posts() ) :
			the_post();

			if ( in_category('column')  ) {
                get_template_part( 'template-parts/content-column', get_post_type() );
            } else {
                get_template_part( 'template-parts/content', get_post_type() );
            }

			the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			//if ( comments_open() || get_comments_number() ) :
			//	comments_template();
			//endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->
	
<script type="text/javascript">
    jQuery(function ($) {
        
        $('.cat-links').each(function(){
            var txt = jQuery(this).html();
            $(this).html(txt.replace(/Posted in/,''));
        });


        //▼ハッシュ付きのページスクロール
        //URLのハッシュ値を取得
        var urlHash = location.hash;
        //ハッシュ値があればページ内スクロール
        if(urlHash) {
            //スクロールを0に戻しておく
            $('body,html').stop().scrollTop(0);
            setTimeout(function () {
                //ロード時の処理を待ち、時間差でスクロール実行
                scrollToAnker(urlHash) ;
            }, 100);
        }
        //通常のクリック時
        $('a[href^="#"]').click(function() {
            //ページ内リンク先を取得
            var href= $(this).attr("href");
            //リンク先が#か空だったらhtmlに
            var hash = href == "#" || href == "" ? 'html' : href;
            //スクロール実行
            scrollToAnker(hash);
            return false;
        });
        // 関数：スムーススクロール
        // 指定したアンカー(#ID)へアニメーションでスクロール
        function scrollToAnker(hash) {
            var target = $(hash);
            var position = target.offset().top;
            $('body,html').stop().animate({scrollTop:position-59}, 500);
        }
        
    });
</script>


<?php
    get_sidebar();
    
    if ( get_current_blog_id() === 1 ) {
        get_footer();
    } elseif ( get_current_blog_id() === 2 ) {
        get_footer("zh");
    }