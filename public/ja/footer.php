<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MTS
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="footer-wrap">
      <div class="footer-wrap-top">
        <div class="footer-container">
          <div class="footer-sitemap">
            <div class="footer-sitemap-nav-block">
              <div class="footer-sitemap-nav-title js-sptoggle">
                <div class="footer-sitemap-nav-title-item">翻訳・通訳サービス</div>
              </div>
              <div class="footer-sitemap-nav-content">
                <ul class="footer-sitemap-nav-list">
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/language">地域別翻訳言語の違い</a>
                  </li>
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/field">翻訳分野</a>
                  </li>
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/price">翻訳料金</a>
                  </li>
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/trial">翻訳トライアル</a>
                  </li>
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/transcription">音声テキスト起こし</a>
                  </li>
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/about/portfolio">翻訳事例紹介</a>
                  </li>
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/interpretation">通訳サービス</a>
                  </li>
                </ul>
              </div>
            </div>
            
            <div class="footer-sitemap-nav-block">
              <div class="footer-sitemap-nav-title js-sptoggle">
                <div class="footer-sitemap-nav-title-item">ゲームローカライズサービス</div>
              </div>
              <div class="footer-sitemap-nav-content">
                <ul class="footer-sitemap-nav-list">
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/game_localization">ゲームローカライズ</a>
                  </li>
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/game_localization/knowhow">美しいインゲームテキスト</a>
                  </li>
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/game_localization/lqa">LQAサービス</a>
                  </li>
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/game_localization/portfolio">ゲーム翻訳実績</a>
                  </li>
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/game_localization/price">ゲーム翻訳料金</a>
                  </li>
                </ul>
              </div>
            </div>
              
            <div class="footer-sitemap-nav-block">
              <div class="footer-sitemap-nav-title js-sptoggle">
                <div class="footer-sitemap-nav-title-item">AI翻訳サービス</div>
              </div>
              <div class="footer-sitemap-nav-content">
                <ul class="footer-sitemap-nav-list">
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/game_localization/ai_translation">ゲームAI翻訳 Mie Smart</a>
                  </li>
                </ul>
              </div>

              <div class="footer-sitemap-nav-title">
                <a class="footer-sitemap-nav-title-link" href="https://taiwantranslation.com/about">私たちについて</a>
              </div>
              <div class="footer-sitemap-nav-title">
                <a class="footer-sitemap-nav-title-link" href="https://taiwantranslation.com/taiwanese">台湾語翻訳のホントのところ</a>
              </div>
              
              <div class="footer-sitemap-nav-title">
                <a class="footer-sitemap-nav-title-link" href="https://taiwantranslation.com/recruit">求人情報</a>
              </div>
              <div class="footer-sitemap-nav-title">
                <a class="footer-sitemap-nav-title-link" href="https://taiwantranslation.com/privacy">セキュリティポリシー</a>
              </div>
            </div>

            <div class="footer-sitemap-nav-block with-image">
              <div class="footer-sitemap-nav-title js-sptoggle">
                <div class="footer-sitemap-nav-title-item">お問い合わせ</div>
              </div>
              <div class="footer-sitemap-nav-content">
                <ul class="footer-sitemap-nav-list">
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/translation_quote">翻訳のお見積り</a>
                  </li>
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/interpretation_quote">通訳のお見積り</a>
                  </li>
                </ul>
                <img loading="lazy" class="contact" src="https://taiwantranslation.com/wp-content/themes/mts/img/img_footer-pop.png" width="50" alt="">
              </div>

              <ul class="footer-sitemap-sns flex flex-align-center flex-justify-center mt-5x">
                <li class="footer-sitemap-sns-item">
                  <a class="footer-sitemap-sns-link" href="https://www.facebook.com/MieManagementService/" target="_blank"><img src="https://taiwantranslation.com/wp-content/themes/mts/img/icon_facebook.png" alt="中国語の翻訳会社Mie Translation Servicesのfacebook"></a>
                </li>
                <li class="footer-sitemap-sns-item">
                  <a class="footer-sitemap-sns-link" href="https://www.instagram.com/mietranslation/" target="_blank"><img src="https://taiwantranslation.com/wp-content/themes/mts/img/icon_instagram.png" alt="中国語の翻訳会社Mie Translation ServicesのInstagram"></a>
                </li>                    
                <li class="footer-sitemap-sns-item">
                  <a class="footer-sitemap-sns-link" href="https://zw.linkedin.com/company/taiwantranslation" target="_blank"><img src="https://taiwantranslation.com/wp-content/themes/mts/img/icon_linkedin.png" alt="中国語の翻訳会社Mie Translation Servicesのlinkdin"></a>
                </li>
                <li class="footer-sitemap-sns-item">
                  <a class="footer-sitemap-sns-link" href="https://www.youtube.com/channel/UCo1oQflk1GkJaiz0-SprQlg/" target="_blank"><img src="https://taiwantranslation.com/wp-content/themes/mts/img/icon_youtube.png" alt="中国語の翻訳会社Mie Translation Servicesのyoutube"></a>
                </li>
              </ul>
            </div>

          </div><!-- .footer-nav -->
        </div>
      </div>
      <div class="footer-container pt-8x">
        <div class="clearfix">
          <div class="col-sm-5 pd-horizon-clear">
            <div class="footer-mie">
              <div class="footer-mie-logo">
                <a href="https://taiwantranslation.com/">
                  <img src="https://taiwantranslation.com/wp-content/themes/mts/img/logo_mts-footer.png" alt="中国語（台湾語）と日本語の翻訳会社｜米耶翻譯股份有限公司 Mie Translation Services">
                </a>
              </div>
              <div class="footer-mie-address">
                住所：10563 台湾台北市松山区光復南路65号2F
              </div>
              <div class="footer-mie-tel">
                <span class="disp-large">
                  電話：日本から 06-6195-4750<br>
                  台湾から 02-2765-2925
                </span>
                <span class="disp-small">
                  電話：日本から <a href="tel:06-6195-4750">06-6195-4750</a><br>
                  台湾から <a href="tel:02-2765-2925">02-2765-2925</a>
                </span>
              </div>
            </div><!-- .footer-mie -->
          </div>
          <div class="col-sm-7 pd-horizon-clear">
            <div class="disp-large">
              <div class="footer-iso flex">
                <a class="mr-5x" href="https://taiwantranslation.com/about/iso17100">
                    <img src="https://taiwantranslation.com/wp-content/themes/mts/img/iso_logo_color.png" alt="翻訳サービスの国際規格ISO17100認証を取得しています" width="80">
                </a>
                <div class="footer-iso-des">
                  <div class="text-weight-bold mb-2x mt-1x">翻訳サービスの国際規格ISO17100認証を取得しています</div>
                  <div style="margin-left: 5em; text-indent: -5em;">
                    対象分野：金融・経済・法務、医学・医薬、<br>工業・科学技術、ゲーム・観光など<br>
                  </div>
                  <div>対象言語：中日・日中、中英・英中</div>
                </div>
              </div>
            </div>
            <div class="disp-small">
              <div class="footer-iso mt-5x">
                <div class="flex flex-align-center mb-5x">
                  <img class="mr-4x" src="https://taiwantranslation.com/wp-content/themes/mts/img/iso_logo_color.png" alt="" width="50">
                  <div class="text-weight-bold">翻訳サービスの国際規格ISO17100認証を取得しています</div>
                </div>
                <div style="margin-left: 5em; text-indent: -5em;">
                  対象分野：金融・経済・法務、医学・医薬、<br>工業・科学技術、ゲーム・観光など<br>
                </div>
                <div>対象言語：中日・日中、中英・英中</div>
              </div>
            </div>
            <div class="text-size-tiny mt-2x">
              ※弊社が提供するすべての翻訳サービスがISO17100に準拠しているわけではありません。
            </div>
          </div>
        </div>

        <p class="site-footer-copy">
          Mie Translation Services Co.,Ltd. All rights reserved.
        </p>
      </div><!-- .site-footer__inner -->
    </footer><!-- #colophon -->
</div><!-- #page -->

<script src="<?php bloginfo('template_directory'); ?>/js/main.js"></script>

<?php wp_footer(); ?>

</body>
</html>