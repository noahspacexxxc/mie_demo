<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MTS
 */

if ( get_current_blog_id() === 1 ) {
    get_header();
} elseif ( get_current_blog_id() === 2 ) {
    get_header("zh");
}

?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">

        <?php if ( have_posts() ) : ?>

        <div class="c-sub-hero disp-ja" style="display: none;">
            <img class="c-sub-hero__image disp-large" src="/wp-content/themes/mts/img/hero_lower.png" alt="">
            <img class="c-sub-hero__image disp-small" src="/wp-content/themes/mts/img/hero_lower_sp.png" alt="">
        </div>
        <div class="c-sub-hero disp-zh" style="display: none;">
            <img class="c-sub-hero__image disp-large" src="/wp-content/themes/mts/img/zh/hero_lower_zh.png" alt="">
            <img class="c-sub-hero__image disp-small" src="/wp-content/themes/mts/img/zh/hero_lower_sp_zh.png" alt="">
        </div>


        <?php if ( is_category('column') ) { ?>
            <div class="c-page-heading top-news__heading news__heading">コラム</div>
        <?php } elseif ( is_category('event') ) { ?>
            <div class="c-page-heading top-news__heading news__heading">イベント関連</div>
        <?php } elseif ( is_category('service') ) { ?>
            <div class="c-page-heading top-news__heading news__heading">サービス</div>
        <?php } else { ?>
            <div class="c-page-heading top-news__heading news__heading">NEWS</div>
        <?php } ?>

        <ul class="top-news-list news-list">


            <?php
            /* Start the Loop */
            while ( have_posts() ) :
            the_post();

            /*
				get_template_part( 'template-parts/content', get_post_type() );
                */
            ?>

            <?php
            if ( get_current_blog_id() === 1 ) {
            ?>
            <li class="top-news-list__item news-list__item">
                <div class="top-news-list__date"><time><?php the_time('Y-m-d'); ?></time></div>
                <p class="top-news-list__title"><a class="top-news-list__title-link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
                <p class="top-news-list__more"><a class="top-news-list__more-link" href="<?php the_permalink(); ?>">詳しく見る &gt;</a></p>
            </li>


            <?php
            } elseif ( get_current_blog_id() === 2 ) {
            ?>

            <li class="top-news-list__item news-list__item">
                <div class="top-news-list__date"><time><?php the_time('Y-m-d'); ?></time></div>
                <p class="top-news-list__title"><a class="top-news-list__title-link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
                <div class="top-news-list__excerpt"><a class="top-news-list__excerpt-link" href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a></div>
                <p class="top-news-list__more"><a class="top-news-list__more-link" href="<?php the_permalink(); ?>">瞭解更多 &gt;</a></p>
            </li>


            <?php
            }
            ?>





            <?php
            endwhile;

            the_posts_navigation();

            else :

            get_template_part( 'template-parts/content', 'none' );

            endif;
            ?>

        </ul><!-- /news-list -->


    </main><!-- #main -->
</div><!-- #primary -->

<?php
if ( get_current_blog_id() === 1 ) {
    get_footer();
} elseif ( get_current_blog_id() === 2 ) {
    get_footer("zh");
}

