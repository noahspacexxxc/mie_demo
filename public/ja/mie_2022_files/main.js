// download banner
if(document.getElementById('downloadBanner')){
  window.onscroll = function () {
    let downloadBanner = document.getElementById('downloadBanner');
    let ctaHeight = (document.querySelector('cta-wrap')) ? document.querySelector('.cta-wrap').offsetHeight : 0;
    let footerHeight = document.getElementById('colophon').offsetHeight

    let check = window.pageYOffset ;
    let docHeight = document.documentElement.offsetHeight;
    let dispHeight = window.innerHeight;

    if(check > docHeight - dispHeight - (ctaHeight + footerHeight)){
      downloadBanner.classList.add('fade-out');
      downloadBanner.classList.remove('fade-in');
    } else {
      downloadBanner.classList.add('fade-in');
      downloadBanner.classList.remove('fade-out');
    }
  };
}

