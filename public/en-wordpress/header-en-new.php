<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MTS
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="format-detection" content="telephone=no">
<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<link rel="shortcut icon" href="/wp-content/themes/mts/favicon.ico">
<link rel="stylesheet" href="/wp-content/themes/mts/bootstrap.css">
<link rel="stylesheet" href="/wp-content/themes/mts/style-en.css">
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<!-- 管理画面ログイン時の上部メニュー分の削除 -->
<style>
html {margin-top: 0 !important;}
</style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-5017112-4"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-5017112-4');
</script> -->
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MS42KD');</script>
<!-- End Google Tag Manager -->

<script type="application/ld+json">
  {
  "@context" : "https://schema.org",
  "@type" : "Corporation",
  "name" : "Mie Translation Services Co.,Ltd.",
  "url" : "https://taiwantranslation.com/en/",
  "logo": "https://taiwantranslation.com/wp-content/themes/mts/img/en/logo.png",
  "telephone" : "+886-2-2765-2925",
  "address" : {
      "@type" : "PostalAddress",
      "streetAddress" :"2F., No.65, Guangfu S. Rd.",
      "addressLocality" : "Songshan Dist.",
      "addressRegion" : "Taipei City",
      "postalCode" : "10563",
      "addressCountry" :"TW"
      },
  "taxID": "28718476",
  "openinghours" : "Mo, Tu, We, Th, Fr 9:00-18:00"
  }
</script>


<?php wp_head(); ?>    
</head>

<body <?php body_class(); ?>>
   
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MS42KD"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
      
   
  <div id="page" class="site site-en">
    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'mts' ); ?></a>

    <div class="header">
      <nav class="navbar" role="navigation">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="menu-container js_nav-item">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="toggle-icon"></span>
            </button>

            <!-- Logo -->
            <div class="logo">
              <a class="logo-wrap" href="http://taiwantranslation.com/en/">
                <img class="logo-img logo-img-main" src="/wp-content/themes/mts/img/en/logo.png" alt="">
                <img class="logo-img logo-img-active logo-n" src="/wp-content/themes/mts/img/en/logo.png" alt="">
              </a>
            </div>
            <!-- End Logo -->
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse nav-collapse">
            <div class="menu-container">
              <ul class="nav navbar-nav navbar-nav-right">
                <li class="js_nav-item nav-item">
                  <a class="nav-item-child nav-item-hover" href="https://taiwantranslation.com/en/game_localization">Game Localization</a>
                </li>
                <li class="js_nav-item nav-item">
                  <?php if(is_page(2)) { ?>
                    <a class="nav-item-child nav-item-hover" href="#expertise">Our Expertise</a>
                  <?php } else { ?>
                    <a class="nav-item-child nav-item-hover" href="https://taiwantranslation.com/en/#expertise">Our Expertise</a>
                  <?php } ?>
                </li>
                <li class="js_nav-item nav-item">
                  <?php if(is_page(2)) { ?>
                    <a class="nav-item-child nav-item-hover" href="#price">Pricing</a>
                  <?php } else { ?>
                    <a class="nav-item-child nav-item-hover" href="https://taiwantranslation.com/en/#price">Pricing</a>
                  <?php } ?>
                </li>
                <li class="js_nav-item nav-item">
                  <a class="nav-item-child nav-item-hover" href="https://taiwantranslation.com/en/about">About us</a>
                </li>
                <li class="js_nav-item nav-item">
                  <?php if(is_page(2)) { ?>
                    <a href="#contact" class="nav-item-child nav-item-hover orange-btn">FREE QUOTE</a>
                  <?php } else { ?>
                    <a href="https://taiwantranslation.com/en/#contact" class="nav-item-child nav-item-hover orange-btn">FREE QUOTE</a>
                  <?php } ?>
                </li>
                <div class="lang-select dropdown hide-phone">
                  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                    <img src="/wp-content/themes/mts/img/en/icon-lang.png" alt="">
                  </button>
                  <ul class="dropdown-menu">
                    <li><a href="#">ENGLISH</a></li>
                    <li><a href="https://taiwantranslation.com/zh" target="_blank">中文</a></li>
                    <li><a href="https://www.taiwantranslation.com/" target="_blank">日本語</a></li>
                  </ul>
                </div>
              </ul>
            </div>
           </div>
          <!-- End Navbar Collapse -->
          <div class="lang-select dropdown show-phone">
            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
              <img src="/wp-content/themes/mts/img/en/icon-lang.png" alt="">
            </button>
            <ul class="dropdown-menu">
              <li><a href="https://en.taiwantranslation.com/">ENGLISH</a></li>
              <li><a href="https://taiwantranslation.com/zh">中文</a></li>
              <li><a href="https://taiwantranslation.com/">日本語</a></li>
            </ul>
          </div>
        </div>
      </nav>
    </div><!-- #header -->

    <div id="content" class="site-content">
      <div>
        <img class="" src="/wp-content/themes/mts/img/en/hero_lower_en.png" alt="Professional Translation Services">
      </div>
