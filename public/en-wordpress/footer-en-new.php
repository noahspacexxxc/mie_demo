<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MTS
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="footer-wrap">
      <div class="footer-container container">
        <div class="clearfix">
          <div class="col-sm-6">
            <div class="footer-mie">
              <div class="footer-mie-logo">
                <a href="https://taiwantranslation.com/en">
                  <img src="/wp-content/themes/mts/img/en/en_footer_logo.png" alt="Mie Translation Services Co.,Ltd.">
                </a>
              </div>
              <div class="footer-mie-address">
                2F., No.65, Guangfu S. Rd., Songshan Dist., Taipei City 105, Taiwan
              </div>
              <div>
                +886(Taiwan)-2-2765-2925<br>
                +81(Japan) 6-6195-4750<br>
                <img src="/wp-content/themes/mts/img/en/email_info.png">
              </div>
            </div><!-- .footer-mie -->
          </div>
          <div class="col-sm-6">
            <div class="hidden-xs">
              <div class="footer-iso flex">
                <a class="mr-5x" href="https://taiwantranslation.com/en/about#iso17100">
                  <img src="https://taiwantranslation.com/wp-content/themes/mts/img/iso_logo_color.png" alt="ISO-Certified Translation Company" width="80">
                </a>
                <div class="footer-iso-des">
                  <div class="text-weight-bold mb-2x mt-1x">ISO-Certified Translation Company</div>
                  <div>
                    The following translation services:<br>Industry, Manufacturing, Game, IT system,<br>
                    Tourism, Marketing and Others
                  </div>
                  <div>The following languages: En <-> Cht, Cht <-> Jp</div>
                </div>
              </div>
            </div>
            <div class="visible-xs">
              <div class="footer-iso mt-5x">
                <div class="flex flex-align-center mb-5x">
                  <img class="mr-4x" src="https://taiwantranslation.com/wp-content/themes/mts/img/iso_logo_color.png" alt="" width="50">
                  <div class="text-weight-bold">
                    ISO-Certified Translation Company
                  </div>
                </div>
                <div>
                  The following translation services:<br>Industry, Manufacturing, Game, IT system,<br>
                  Tourism, Marketing and Others
                </div>
                <div>The following languages: En <-> Cht, Cht <-> Jp</div>
              </div>
            </div>
            <div class="text-size-tiny mt-2x">
              Not all translation services provided by our company comply with ISO 17100.
            </div>
          </div>
        </div>

        <p class="site-footer-copy">
          Mie Translation Services Co.,Ltd. All rights reserved.
        </p>
      </div><!-- .site-footer__inner -->
    </footer><!-- #colophon -->
</div><!-- #page -->


<script src="/wp-content/themes/mts/js/bootstrap.min.js"></script>
<script type="text/javascript">
  $(document).ready(function($){
    $(function() {
      $('a[href^="#"]').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          let target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html,body').animate({
              scrollTop: target.offset().top - 87
            }, 800);
            return false;
          }
        }
      });


      if(location.hash) {
        let target = $(location.hash);
        $('html,body').animate({
          scrollTop: target.offset().top - 87
        }, 300);
        return false;
      }
    });
  });
</script>
<?php wp_footer(); ?>

</body>
</html>