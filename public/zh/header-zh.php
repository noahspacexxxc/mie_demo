<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MTS
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="format-detection" content="telephone=no">
<link href="https://fonts.googleapis.com/css?family=Roboto|Philosopher&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<link rel="shortcut icon" href="/wp-content/themes/mts/favicon.ico">
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-5017112-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-5017112-1');
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MS42KD');</script>
<!-- End Google Tag Manager -->
<!-- 管理画面ログイン時の上部メニュー分の削除 -->
<style>
html {margin-top: 0 !important;}
</style>

<script type="application/ld+json">
  {
  "@context" : "https://schema.org",
  "@type" : "Corporation",
  "name" : "米耶翻譯股份有限公司",
  "url" : "https://taiwantranslation.com/zh/",
  "logo": "https://taiwantranslation.com/wp-content/themes/mts/img/logo_mts-header.png",
  "telephone" : "+886-2-2765-2925",
  "address" : {
      "@type" : "PostalAddress",
      "streetAddresss" :"光復南路65號2樓",
      "addressLocality" : "松山區",
      "addressRegion" : "台北市",
      "postalCode" : "10563",
      "addressCountry" :"TW"
      },
  "taxID": "28718476",
  "openinghours" : "Mo, Tu, We, Th, Fr 9:00-18:00"
  }
</script>



<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site site-zh">
   
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MS42KD"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'mts' ); ?></a>

	<header id="masthead" class="site-header">
		
        <!-- ▼Site Logo -->
        <div class="site-branding">
			<a href="http://taiwantranslation.com/zh/" class="custom-logo-link" rel="home">
				<img src="/wp-content/themes/mts/img/logo_mts-header.png" alt="米耶翻譯股份有限公司　Mie Translation Services Co.,Ltd.">
			</a>				
		</div>
        
        
        <!-- ▼Main  navigation  -->
        <div class="c-overlay"></div>
        <div class="menu-toggle"><span></span></div>
        <nav id="site-navigation" class="main-navigation">
            <div class="main-navigation__inner">
                <!-- スマホ用メニューボタン -->
                <!-- <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><span></span></button> -->      
                


                <!-- メニューリスト -->
                <?php
                wp_nav_menu( array(
                    'theme_location' => 'menu-1',
                    'menu_id'        => 'primary-menu',
                    'menu_class'        => 'nav-menu',
                ) );
                ?>

                
                <div class="site-language-sp">
                  <div class="site-language__btn">
                    <div class="site-language__link">LANGUAGE</div>
                    <ul class="site-language-list">
                      <li class="site-language-list__item">
                        <a href="https://taiwantranslation.com/" title="日本語">日本語</a>
                      </li>
                      <li class="site-language-list__item">
                        <a href="https://taiwantranslation.com/zh/" title="中文" class="current_language">中文</a>
                      </li>
                      <li class="site-language-list__item">
                        <a href="https://taiwantranslation.com/en/" title="English">English</a>
                      </li>
                    </ul>
                  </div>
                  </div>
                <div class="menubnr-sp">
                  <ul class="menubnr-sp-list">
                    <li class="menubnr-sp-list__item">
                      <a class="menubnr-sp-list__link menubnr_sp-list-e-learning" href="https://taiwantranslation.com/zh/e-learning">
                        <img class="menubnr-sp-list__image menubnr_sp-list-img" src="https://taiwantranslation.com/zh/wp-content/uploads/sites/2/2022/03/icon_learning.png">
                        <div>翻譯課程</div>
                      </a>
                    </li>
                  </ul>
                </div>
            </div><!-- .main-navigation__inner -->
            
		</nav><!-- #site-navigation -->
        
        
        <!-- Language 切替スイッチャー -->
        <!-- <div class="site-language">
            <?php if ( function_exists( 'the_msls' ) ) the_msls(); ?>
        </div> -->
        <div class="site-language">
          <div class="site-language__btn">
            <div class="site-language__link">
              <img class="mb-2x" width="45px" src="https://taiwantranslation.com/wp-content/themes/mts/img/icon-lang.png" alt="">
            </div>
            <ul class="site-language-list">
              <li class="site-language-list__item text-center">
                <a href="https://taiwantranslation.com/" title="日本語">日本語</a>
              </li>
              <li class="site-language-list__item">
                <a href="https://taiwantranslation.com/en/" title="English">English</a>
              </li>
            </ul>
          </div>
        </div>
        
        <!-- TEL block -->
        <div class="site-header-tel">
            Tel : 02-2765-2925
        </div>

        
        <!-- ▼Contact Navi-->
        <nav id="site-navigation-contact" class="contact-nav">
            <ul class="contact-nav-list">
                <li class="contact-nav-list__item contact-nav-list__item--contact">
                    <div class="contact-nav-list__link">
                        <img class="contact-nav-list__image disp-large-ib" src="<?php bloginfo('template_directory'); ?>/img/zh/text_head-contact__zh.png" alt="線上估價">
                        <img class="contact-nav-list__image disp-small-ib" src="<?php bloginfo('template_directory'); ?>/img/icon_mitsumori.png" alt="線上估價">
                    </div>
                    <ul class="contact-nav-sublist">
                        <li class="contact-nav-sublist__item">
                            <a class="contact-nav-sublist__link" href="/zh/translation_quote">筆譯服務</a>
                        </li>
                        <li class="contact-nav-sublist__item">
                            <a class="contact-nav-sublist__link" href="/zh/interpretation_quote">口譯服務</a>
                        </li>
                    </ul> 
                </li>
                <li class="contact-nav-list__item contact-nav-list__item--e-learning">
                    <a class="contact-nav-list__link" href="https://taiwantranslation.com/zh/e-learning"><img class="contact-nav-list__image" src="https://taiwantranslation.com/zh/wp-content/uploads/sites/2/2022/03/icon_learning.png" alt="">&nbsp;翻譯課程</a>
                </li>
                <!--
                <li class="contact-nav-list__item contact-nav-list__item--freelance">
                    <a class="contact-nav-list__link" href=""><img class="contact-nav-list__image" src="<?php bloginfo('template_directory'); ?>/img/text_head-freelance.png" alt=""></a>
                </li>
                -->
            </ul>
        </nav>
        
        <script type="text/javascript">
          
          jQuery(function ($) {
              // メニュードロップダウンメニュー
              if ($(window).width() > 1199) {
                $('.main-navigation ul.nav-menu > li, .main-navigation ul.nav-menu .menu-item-has-children').hover(
                  function() {
                    //カーソルが重なった時
                    $(this).children('.sub-menu').addClass('active');
                  }, function() {
                    //カーソルが離れた時
                   $(this).children('.sub-menu').removeClass('active');
                  }
                );
              }
              
              // メニュードロップダウンメニュー
              if ($(window).width() < 1200) {
                $(function(){
                  $(".main-navigation ul.nav-menu > li a").on("click", function(e) {
                    if(e.currentTarget.parentElement.id === "menu-item-841") {
                      return
                    }
                    $(this).next().slideToggle();
                    $(this).toggleClass("active");
                  });
                });
              }
              
              // 言語切替用のドロップダウンメニュー
              $('.site-language__btn').hover(
                function() {
                  //カーソルが重なった時
                  $(this).children('.site-language-list').addClass('active');
                }, function() {
                  //カーソルが離れた時
                 $(this).children('.site-language-list').removeClass('active');
                }
              );
              
              // お問い合わせ用のドロップダウンメニュー
              $('.contact-nav-list__item').hover(
                function() {
                  //カーソルが重なった時
                  $(this).children('.contact-nav-sublist').addClass('active');
                }, function() {
                  //カーソルが離れた時
                 $(this).children('.contact-nav-sublist').removeClass('active');
                }
              );
              
              // スマホ用メニュー
              $('.menu-toggle').click(function(){
                if($(this).hasClass("toggled")){ 
                  $(this).removeClass("toggled");
                  $('#site-navigation').removeClass("toggled");    
                  $('.c-overlay').hide();  
                }else{
                  $(this).addClass("toggled");
                  $('#site-navigation').addClass("toggled");
                  $('.c-overlay').show();
                }
              });
              // オーバーレイから閉じる
              $('.c-overlay').click(function(){
                if($('.menu-toggle').hasClass("toggled")){ 
                  $('.menu-toggle').removeClass("toggled");
                  $('#site-navigation').removeClass("toggled");    
                  $('.c-overlay').hide();  
                }else{
                  $('.menu-toggle').addClass("toggled");
                  $('#site-navigation').addClass("toggled");
                }
              });
              // スマホ用フッターメニュー
              if ($(window).width() < 992) {
                $(function(){
                  $('.js-sptoggle').on("click", function() {
                    $(this).next().slideToggle();
                    $(this).toggleClass("active");
                  });
                });
              }
              
              // 言語切り替えスイッチャーの位置調整（Englishを最後に）
              // $('.site-language-list__item:last-child').after($('.site-language-list__item:first-child'));
              // $('.site-language-list__item a[href*="https://en.taiwantranslation.com"]').hide();
              
          });
        </script>
        
        
	</header><!-- #masthead -->

	<div id="content" class="site-content">
