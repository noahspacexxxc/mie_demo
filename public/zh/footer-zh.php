<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MTS
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="footer-wrap">
    <div class="footer-wrap-top">
      <div class="footer-container">
        <div class="footer-sitemap">
            <div class="footer-sitemap-nav-block">
              <div class="footer-sitemap-nav-title js-sptoggle">
                <div class="footer-sitemap-nav-title-item">筆譯服務</div>
              </div>
              <div class="footer-sitemap-nav-content">
                <ul class="footer-sitemap-nav-list">
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/zh/translation">筆譯服務</a>
                  </li>
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/zh/field">筆譯服務領域</a>
                  </li>
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/zh/proofreading">日文潤稿與文案撰寫服務</a>
                  </li>
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/zh/membership">會員方案</a>
                  </li>
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/zh/trial">試譯服務</a>
                  </li>
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/zh/transcription">日文聽打服務</a>
                  </li>
                </ul>
              </div>
            </div>
            
            <div class="footer-sitemap-nav-block">
              <div class="footer-sitemap-nav-title js-sptoggle">
                <div class="footer-sitemap-nav-title-item">遊戲本地化</div>
              </div>
              <div class="footer-sitemap-nav-content">
                <ul class="footer-sitemap-nav-list">
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/zh/game_localization">遊戲本地化</a>
                  </li>
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/zh/game_localization/knowhow">高品質的理由</a>
                  </li>
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/zh/game_localization/lqa">LQA語言測試服務</a>
                  </li>
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/zh/game_localization/portfolio">本地化成果</a>
                  </li>
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/zh/game_localization/price">本地化語言與費用</a>
                  </li>
                </ul>
              </div>

              <div class="footer-sitemap-nav-title js-sptoggle">
                <div class="footer-sitemap-nav-title-item">口譯服務</div>
              </div>
              <div class="footer-sitemap-nav-content">
                <ul class="footer-sitemap-nav-list">
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/zh/interpretation">口譯服務類型及費用</a>
                  </li>
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/zh/interpretation/interpreter">口譯人員簡歷</a>
                  </li>
                </ul>
              </div>
            </div>
              
            <div class="footer-sitemap-nav-block">
               <div class="footer-sitemap-nav-title">
                <a class="footer-sitemap-nav-title-link" href="https://taiwantranslation.com/zh/about">關於我們</a>
              </div>
              <div class="footer-sitemap-nav-title">
                <a class="footer-sitemap-nav-title-link" href="https://taiwantranslation.com/zh/recruit">人才招募</a>
              </div>
              <div class="footer-sitemap-nav-title">
                <a class="footer-sitemap-nav-title-link" href="https://taiwantranslation.com/zh/e-learning">線上翻譯課程</a>
              </div>
              <div class="footer-sitemap-nav-title">
                <a class="footer-sitemap-nav-title-link" href="https://taiwantranslation.com/zh/privacy">隱私權政策</a>
              </div>
            </div>

            <div class="footer-sitemap-nav-block with-image">
              <div class="footer-sitemap-nav-title js-sptoggle">
                <div class="footer-sitemap-nav-title-item">線上估價</div>
              </div>
              <div class="footer-sitemap-nav-content">
                <ul class="footer-sitemap-nav-list">
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/zh/translation_quote">筆譯服務</a>
                  </li>
                  <li class="footer-sitemap-nav-list-item">
                    <a class="footer-sitemap-nav-list-link" href="https://taiwantranslation.com/zh/interpretation_quote">口譯服務</a>
                  </li>
                </ul>
              </div>

              <ul class="footer-sitemap-sns flex flex-align-center flex-justify-center mt-5x">
                <li class="footer-sitemap-sns-item">
                  <a class="footer-sitemap-sns-link" href="https://www.facebook.com/MieManagementService/" target="_blank"><img src="https://taiwantranslation.com/wp-content/themes/mts/img/icon_facebook.png" alt="米耶翻譯股份有限公司 Mie Translation Services facebook"></a>
                </li>
                <li class="footer-sitemap-sns-item">
                  <a class="footer-sitemap-sns-link" href="https://www.instagram.com/mietranslation/" target="_blank"><img src="https://taiwantranslation.com/wp-content/themes/mts/img/icon_instagram.png" alt="米耶翻譯股份有限公司 Mie Translation Services Instagram"></a>
                </li>                    
                <li class="footer-sitemap-sns-item">
                  <a class="footer-sitemap-sns-link" href="https://zw.linkedin.com/company/taiwantranslation" target="_blank"><img src="https://taiwantranslation.com/wp-content/themes/mts/img/icon_linkedin.png" alt="米耶翻譯股份有限公司 Mie Translation Services linkdin"></a>
                </li>
                <li class="footer-sitemap-sns-item">
                  <a class="footer-sitemap-sns-link" href="https://www.youtube.com/channel/UCo1oQflk1GkJaiz0-SprQlg/" target="_blank"><img src="https://taiwantranslation.com/wp-content/themes/mts/img/icon_youtube.png" alt="米耶翻譯股份有限公司 Mie Translation Services youtube"></a>
                </li>
              </ul>
            </div>

          </div><!-- .footer-nav -->
      </div>
    </div>
    <div class="footer-container pt-8x">
      <div class="clearfix">
        <div class="col-sm-5 pd-horizon-clear">
          <div class="footer-mie mb-5x">
            <div class="footer-mie-logo">
              <a href="https://taiwantranslation.com/zh">
                <img src="https://taiwantranslation.com/wp-content/themes/mts/img/logo_mts-footer.png" alt="米耶翻譯股份有限公司 Mie Translation Services">
              </a>
            </div>
            <div class="footer-mie-address">
              地址：10563台北市松山區光復南路65號2樓
            </div>
            <div class="">
              <span class="disp-large">
                TEL：02-2765-2925
              </span>
              <span class="disp-small">
                TEL：<a href="tel:02-2765-2925">02-2765-2925</a>
              </span>
            </div>
          </div><!-- .footer-mie -->
        </div>
        <div class="col-sm-7 pd-horizon-clear">
          <div class="disp-large">
            <div class="footer-iso flex">
              <a href="https://taiwantranslation.com/zh/iso17100">
                <img class="mr-5x" src="https://taiwantranslation.com/wp-content/themes/mts/img/iso_logo_color.png" alt="" width="80">
              </a>
              <div class="footer-iso-des">
                <div class="text-weight-bold mb-2x mt-1x">取得翻譯服務國際標準ISO17100認證</div>
                <div style="margin-left: 3em; text-indent: -3em;">
                  領域：金融、經濟、法務、醫學、醫藥、<br>工業、科技、遊戲、觀光等
                </div>
                <div>語言：中日・日中、中英・英中</div>
              </div>
            </div>
          </div>
          <div class="disp-small">
            <div class="footer-iso">
              <div class="flex flex-align-center mb-5x">
                <a href="https://taiwantranslation.com/zh/iso17100">
                  <img class="mr-4x" src="https://taiwantranslation.com/wp-content/themes/mts/img/iso_logo_color.png" alt="" width="50">
                </a>
                <div class="text-weight-bold">取得翻譯服務國際標準ISO17100認證</div>
              </div>
              <div style="margin-left: 5em; text-indent: -5em;">
                領域：金融、經濟、法務、醫學、醫藥、<br>工業、科技、遊戲、觀光等<br>
              </div>
              <div>語言：中日・日中、中英・英中</div>
            </div>
          </div>
          <div class="text-size-tiny mt-2x">
            ※並非所有的翻譯服務均遵循ISO17100標準
          </div>
        </div>
      </div>

      <p class="site-footer-copy">
        Mie Translation Services Co.,Ltd. All rights reserved.
      </p>
    </div><!-- .site-footer__inner -->
  </footer><!-- #colophon -->
</div><!-- #page -->



<?php wp_footer(); ?>

</body>
</html>